import os
import cv2 as cv

import numpy as np
import torchvision

from src.utils.utils import json_load
from src.utils.joints import JointInfo as Joints
from src.dataset.dataset_reader import DatasetReader
from src.utils.utils import kp3d_to_kp2d
import pdb

import random

import torch
from manopth.manolayer import ManoLayer

from src.dataset.transforms import Augmentation


size_train = 91168
size_val = 19536
# = 15000 + 4536
reordered_val_seq = [i for i in range(size_val)]
random.Random(12345).shuffle(reordered_val_seq)
print(reordered_val_seq[:50])
            
class FreiHANDDataset(DatasetReader):
    def __init__(self, split, data_transforms, dataset_path, train_all=False):
        self.train_all = train_all
        # NOTE You may want to extend functionality such that you train on both
        # validation and training data for the final performance
        dataset_name = "FreiHAND"
        super().__init__(dataset_name, dataset_path, data_transforms)
        self.train_size = 0
        # Convert order to AIT order and convert to milimeters
        if split == "test":
            k_path = os.path.join(dataset_path, f"{split}_K.json")
            K = np.array(json_load(k_path))
            K = np.tile(K, (4, 1, 1))
            kp3d = np.zeros((len(K), 21, 3))
            mano_pose = np.zeros((len(K), 48))
            mano_shape = np.zeros((len(K), 10))
            verts = np.zeros((len(K), 778, 3))
        else:
            kp3d, K, mano_pose, mano_shape, verts = self.load_split(dataset_path, split)
            if self.train_all and split == 'train':
#                 if split == 'val':
#                     pdb.set_trace()
                self.train_size = kp3d.shape[0]
                kp3d_, K_, mano_pose_, mano_shape_, verts_ = self.load_split(dataset_path, 'val')
                
                # taking another 15000 samples from validation set
                kp3d = np.append(kp3d, kp3d_[reordered_val_seq[:15000]], axis=0)
                K = np.append(K, K_[reordered_val_seq[:15000]], axis=0)
                mano_pose = np.append(mano_pose, mano_pose_[reordered_val_seq[:15000]], axis=0)
                mano_shape = np.append(mano_shape, mano_shape_[reordered_val_seq[:15000]], axis=0)
                verts = np.append(verts, verts_[reordered_val_seq[:15000]], axis=0)
            elif self.train_all and split == 'val':
                kp3d = kp3d[reordered_val_seq[15000:]]
                K = K[reordered_val_seq[15000:]] 
                mano_pose = mano_pose[reordered_val_seq[15000:]]
                mano_shape = mano_shape[reordered_val_seq[15000:]]
                verts = verts[reordered_val_seq[15000:]]

        self.kp3d = kp3d
        self.K = K
        self.split = split
        self.mano_pose = mano_pose
        self.mano_shape = mano_shape
        self.verts = verts
        
        # ####################################
        
        self.is_train = (split == "train")
        self.augmentor = Augmentation()
        

    def load_split(self, dataset_path, split):
        k_path = os.path.join(dataset_path, f"{split}_K.json")
        K = np.array(json_load(k_path))
        xyz_path = os.path.join(dataset_path, f"{split}_xyz.json")
        # TODO!
        # kp3d = self.convert_order(np.array(json_load(xyz_path))) * 1000
        # kp3d = np.array(json_load(xyz_path)) * 1000
        
        kp3d = self.convert_order(np.array(json_load(xyz_path))) * 1000
        kp3d = np.tile(kp3d, (4, 1, 1))

        K = np.tile(K, (4, 1, 1))
        mano_path = os.path.join(dataset_path, f"{split}_mano.json")
        mano = np.array(json_load(mano_path))
        mano_pose = mano[:, :, :48].squeeze()
        mano_shape = mano[:, :, 48:58].squeeze()
        mano_pose = np.tile(mano_pose, (4, 1))
        mano_shape = np.tile(mano_shape, (4, 1))
        verts_path = os.path.join(dataset_path, f"{split}_verts.json")
        # TODO!
        # verts = np.array(json_load(verts_path)) * 1000
        verts = np.array(json_load(verts_path))
        verts = np.tile(verts, (4, 1, 1))
        return kp3d, K, mano_pose, mano_shape, verts

    def load_sample(self, idx):
        if not self.train_all:
            img_path = os.path.join(self.dataset_path, f"{self.split}", "%.8d.jpg" % idx)
        else:
            if self.split == "train":
                if idx < self.train_size:
                    img_path = os.path.join(self.dataset_path, f"train", "%.8d.jpg" % idx)
                else:
                    img_path = os.path.join(self.dataset_path, f"val", "%.8d.jpg" % (reordered_val_seq[idx - self.train_size]))
                                        # 0 < idx - self.train_size < 15000
            elif self.split == "val":
                img_path = os.path.join(self.dataset_path, f"val", "%.8d.jpg" % (reordered_val_seq[15000:][idx]))
            elif self.split == "test":
                img_path = os.path.join(self.dataset_path, f"test", "%.8d.jpg" % idx)
            

        kp3d = self.kp3d[idx]
        K = self.K[idx]
        kp2d = kp3d_to_kp2d(kp3d, K)
        # kp3d_new = np.ones((21, 4))
        # kp3d_new[:, :-1] = kp3d
        # kp2d_new = np.ones((21, 3))
        # kp2d_new[:, :-1] = kp2d
        mano_pose = self.mano_pose[idx]
        mano_shape = self.mano_shape[idx]
        verts = self.verts[idx]
        # Load image
        img = cv.imread(img_path)
        # Convert from BGR to RGB
        img = cv.cvtColor(img, cv.COLOR_BGR2RGB)
        mjm_mask = np.ones((21,1))
        mvm_mask = np.ones((195,1))

        sample = {"image": img, "kp3d": kp3d, "kp2d": kp2d, "K": K, "mano_pose": mano_pose, "mano_shape": mano_shape, "verts": verts, "mjm_mask":mjm_mask, "mvm_mask":mvm_mask}
        if self.is_train:
            toret = self.augmentor(sample)
            return toret
        else:
            return sample
        
    def __len__(self):
        return len(self.kp3d)
    
    ####################################################################

    def convert_order(self, kp3d_from):
        """
        Convert order from FreiHAND to AIT. Accepts batch and sample input
        """
        # order of Freiburg dataset
        # 0: wrist
        # 1 - 4: thumb[palm to tip], ...
        # 5 - 8: index,
        # 9 - 12: middle
        # 13 - 16: ring
        # 17 - 20: pinky,

        output = np.zeros(shape=kp3d_from.shape, dtype=kp3d_from.dtype)

        output[..., Joints.root, :] = kp3d_from[..., 0, :]
        output[..., Joints.thumb_mcp, :] = kp3d_from[..., 1, :]
        output[..., Joints.thumb_pip, :] = kp3d_from[..., 2, :]
        output[..., Joints.thumb_dip, :] = kp3d_from[..., 3, :]
        output[..., Joints.thumb_tip, :] = kp3d_from[..., 4, :]

        output[..., Joints.index_mcp, :] = kp3d_from[..., 5, :]
        output[..., Joints.index_pip, :] = kp3d_from[..., 6, :]
        output[..., Joints.index_dip, :] = kp3d_from[..., 7, :]
        output[..., Joints.index_tip, :] = kp3d_from[..., 8, :]

        output[..., Joints.middle_mcp, :] = kp3d_from[..., 9, :]
        output[..., Joints.middle_pip, :] = kp3d_from[..., 10, :]
        output[..., Joints.middle_dip, :] = kp3d_from[..., 11, :]
        output[..., Joints.middle_tip, :] = kp3d_from[..., 12, :]

        output[..., Joints.ring_mcp, :] = kp3d_from[..., 13, :]
        output[..., Joints.ring_pip, :] = kp3d_from[..., 14, :]
        output[..., Joints.ring_dip, :] = kp3d_from[..., 15, :]
        output[..., Joints.ring_tip, :] = kp3d_from[..., 16, :]

        output[..., Joints.pinky_mcp, :] = kp3d_from[..., 17, :]
        output[..., Joints.pinky_pip, :] = kp3d_from[..., 18, :]
        output[..., Joints.pinky_dip, :] = kp3d_from[..., 19, :]
        output[..., Joints.pinky_tip, :] = kp3d_from[..., 20, :]

        return output


if __name__ == "__main__":
    """
    Visualize one sample
    """
    import matplotlib.pyplot as plt
    from src.utils.vis_utils import plot_fingers
    from mpl_toolkits.mplot3d import Axes3D
    from src.utils.utils import kp3d_to_kp2d

    dataset_path = "/cluster/home/yucjiang/sachan/ct/data/freihand_dataset_MP"
    split = "val"
    data_transform = None

    freihand_dataset = FreiHANDDataset(split, data_transform, dataset_path)

    idx = 426
    sample = freihand_dataset.load_sample(idx)

    freihand_dataset_ = FreiHANDDataset('train', data_transform, dataset_path)
    sample3 = freihand_dataset_.load_sample(idx)

    freihand_dataset_all = FreiHANDDataset('train', data_transform, dataset_path, True)
    sample2 = freihand_dataset_all.load_sample(idx)
    sample4 = freihand_dataset_all.load_sample(idx + len(freihand_dataset_))

    pdb.set_trace()

    img = sample["image"]
    kp3d = sample["kp3d"]
    K = sample["K"]
    kp2d = kp3d_to_kp2d(kp3d, K)  # Project to 2D using pinhole camera model

    fig = plt.figure(figsize=(12, 5))
    ax_rgb = fig.add_subplot(131)
    ax_3d_1 = fig.add_subplot(132, projection="3d")
    ax_3d_2 = fig.add_subplot(133, projection="3d")
    plot_fingers(kp2d, img_rgb=img, ax=ax_rgb)
    plot_fingers(kp3d, ax=ax_3d_1, view=(-90, -90))  # frontal view
    plot_fingers(kp3d, ax=ax_3d_2)  # top view
    plt.show()