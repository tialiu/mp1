import torch
import cv2 as cv  # cv is faster than PIL

import numpy as np
from src.utils.joints import JointInfo

from metro.utils.image_ops import crop, rot_aa, transform
# NOTE Try adding data augmentation here


class NumpyToPytorch:
    def __call__(self, sample):
        # Torch take C x H x W whereas np,cv use H x W x C
        img = sample["image"].transpose(2, 0, 1)
        # Convert to float and map to [0,1]
        sample["image"] = img.astype(np.float32) / 255
        # Transfrom from numpy array to pytorch tensor
        for k, v in sample.items():
            sample[k] = torch.from_numpy(v).float()

        return sample


class Resize:
    """
    Resizes the image to img_size
    """

    def __init__(self, img_size):
        self.img_size = tuple(img_size)

    def __call__(self, sample):
        sample["image"] = cv.resize(sample["image"], self.img_size)

        return sample


class ScaleNormalize:
    """
    Scale normalizes the 3D joint position by the MCP bone of the index finger.
    The resulting 3D joint skeleton has an index MCP bone length of 1
    NOTE: This function will throw a warning for the test data, as the ground-truth
    is set to 0. This is because they are not available.
    """

    def __init__(self):
        # NOTE Try taking the bone as parameter and see if scale normalizing other bones
        # affects performance
        pass

    def __call__(self, sample):
        kp3d = sample["kp3d"]
        bone_length = np.linalg.norm(
            kp3d[JointInfo.index_mcp] - kp3d[JointInfo.index_pip]
        )
        # bone_length = np.ones_like(bone_length)
        kp3d = kp3d / bone_length
        sample["kp3d"] = kp3d
        sample["scale"] = np.array(bone_length)

        return sample

class Augmentation:
    def __init__(self):
        self.noise_factor = 0.4
        self.scale_factor = 0.25
        self.rot_factor = 90

    def __call__(self, sample):
        self.sc = 1.0
        self.pn = np.random.uniform(1-self.noise_factor, 1+self.noise_factor, 3)
        self.rot = min(2*self.rot_factor,
                    max(-2*self.rot_factor, np.random.randn()*self.rot_factor))
        self.sc = min(1+self.scale_factor,
                    max(1-self.scale_factor, np.random.randn()*self.scale_factor+1))
        if np.random.uniform() <= 0.6:
            self.rot = 0
        
        rgb_img = sample['image']
        # image roration
        M = cv.getRotationMatrix2D((112, 112), self.rot, self.sc)
        rgb_img = cv.warpAffine(rgb_img, M, (224, 224))
        # image noise
        rgb_img[:,:,0] = np.minimum(255.0, np.maximum(0.0, rgb_img[:,:,0]*self.pn[0]))
        rgb_img[:,:,1] = np.minimum(255.0, np.maximum(0.0, rgb_img[:,:,1]*self.pn[1]))
        rgb_img[:,:,2] = np.minimum(255.0, np.maximum(0.0, rgb_img[:,:,2]*self.pn[2]))
        sample['image'] = rgb_img

        kp3d = sample['kp3d']
        rot_mat = np.eye(3)
        if not self.rot == 0:
            rot_rad = -self.rot * np.pi / 180
            sn,cs = np.sin(rot_rad), np.cos(rot_rad)
            rot_mat[0,:2] = [cs, -sn]
            rot_mat[1,:2] = [sn, cs]
        kp3d = np.einsum('ij,kj->ki', rot_mat, kp3d)
        sample['kp3d'] = kp3d
        
        verts = sample['verts']        
        verts = np.einsum('ij,kj->ki', rot_mat, verts) 
        sample['verts'] = verts

        kp2d = sample['kp2d']
        ones = np.ones(shape=(kp2d.shape[0], 1))
        points_ones = np.hstack([kp2d, ones])
        kp2d = M.dot(points_ones.T).T
        kp2d /= 224.0
        sample['kp2d'] = kp2d

        mano_pose = sample['mano_pose']
        mano_pose[:3] = rot_aa(mano_pose[:3], self.rot)
        sample['mano_pos'] = mano_pose

        return sample

