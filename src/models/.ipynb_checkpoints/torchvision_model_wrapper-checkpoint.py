"""
Class to that interfaces with various torchvision models
"""
import torch
from torch import nn
import torchvision.models as models
from itertools import accumulate
import os

import torchvision
# from manopth.manolayer import ManoLayer
# from metro.modeling._mano import MANO, Mesh

import logging
# torch.autograd.set_detect_anomaly(True)

def model_list():
    # NOTE You can add other model types found in the torchvision library
    model_list = ["resnet18", "resnet34", "resnet50", "resnet101", "resnet152", 
                  "fcn_resnet50", "fcn_resnet101", "deeplabv3_resnet50","deeplabv3_resnet101",]
    return model_list


class ResNetWrapper(nn.Module):
    """
    This is a resnet wrapper class which takes existing resnet architectures and
    adds a final linear layer at the end, ensuring proper output dimensionality
    """

    def __init__(self, model_name, output_slices):
        super().__init__()

        # Use a resnet-style backend
        if "resnet18" == model_name:
            model_func = models.resnet18
        elif "resnet34" == model_name:
            model_func = models.resnet34
        elif "resnet50" == model_name:
            model_func = models.resnet50
        elif "resnet101" == model_name:
            model_func = models.resnet101
        elif "resnet152" == model_name:
            model_func = models.resnet152
        elif "fcn_resnet50" == model_name:
            model_func = models.segmentation.fcn_resnet50
        elif "fcn_resnet101" == model_name:
            model_func = models.segmentation.fcn_resnet101
        elif "deeplabv3_resnet50" == model_name:
            model_func = models.segmentation.deeplabv3_resnet50
        elif "deeplabv3_resnet101" == model_name:
            model_func = models.segmentation.deeplabv3_resnet101
        else:
            raise Exception(f"Unknown backend model type: {model_name}")

        # Prepare the slicing
        slice_keys = list(output_slices.keys())
        slice_vals = list(output_slices.values())
        cumsum = list(accumulate(slice_vals))  # Hehe funneh name lolz
        # (key, start_idx, to_idx)
        output_idx = list(zip(slice_keys, [0] + cumsum[:-1], cumsum))
        # Construct the encoder.
        # NOTE You may want to look at the arguments of the resnet constructor
        # to test out various things:
        # https://pytorch.org/vision/stable/_modules/torchvision/models/resnet.html
        b_model = model_func(pretrained=True)
        if model_name in ['deeplabv3_resnet101', 'deeplabv3_resnet50', 'fcn_resnet101', 'fcn_resnet50']:
            b_model = b_model.backbone
        encoder = nn.Sequential(
            b_model.conv1,
            b_model.bn1,
            b_model.relu,
            b_model.maxpool,
            b_model.layer1,
            b_model.layer2,
            b_model.layer3,
            b_model.layer4,
            nn.AdaptiveAvgPool2d(output_size=(1, 1)),
        )
        # Construct the final layer
        if model_name in ['deeplabv3_resnet101', 'deeplabv3_resnet50', 'fcn_resnet101', 'fcn_resnet50']:
            feat_dim = 2048
        else:
            feat_dim = b_model.fc.in_features
        logging.info("b_model.fc.in_features = %d"%(feat_dim, ))
        
        self.ncomps = 45
        output_dim = cumsum[-1]
        # output_dim = (self.ncomps + 3) + 10 + 3
        
        final_layer = nn.Sequential(
            nn.Linear(feat_dim, 1024),
            nn.ReLU(),
            nn.Linear(1024, output_dim),
        )

        self.encoder = encoder
        self.final_layer = final_layer
        self.output_idx = output_idx
        
        self.normalize_img = torchvision.transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                         std=[0.229, 0.224, 0.225])

    def forward(self, batch):
        x = self.normalize_img(batch["image"])
        # Get feature output
        f = self.encoder(x)
        # Get final output
        f = f.flatten(start_dim=1)
        out = self.final_layer(f)
        
        # th_verts, th_jtr = self.mano.layer(batch['mano_pose'], batch['mano_shape'])
        # print((th_jtr - batch['kp3d'])[0].view(-1))
        # print(batch['kp3d'][0].view(-1))
        
        # th_pose_coeffs, th_betas, th_trans = torch.split(out, [self.ncomps+3, 10, 3], dim=1)

        # print("\n\n\n\n\n", out[...,:self.ncomps].shape, out[...,self.ncomps:].shape)
        # th_verts, th_jtr = self.mano.layer(th_pose_coeffs, th_betas, root_palm=True)
        # print("\n\n\n\n\n", th_verts.shape, th_jtr.shape)
        
        # Slice the output
        #out_dict = {'kp3d': th_jtr.view(th_jtr.size(0), -1)}
        out_dict = {}
        for key, start_idx, to_idx in self.output_idx:
            out_dict[key] = out[..., start_idx:to_idx]

        return out_dict


def get_model(cfg_model):
    model = ResNetWrapper(
        model_name=cfg_model.name, output_slices=cfg_model.output_slices
    )

    return model
