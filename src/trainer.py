import torch
import os
import json
from tqdm import tqdm
import subprocess
import collections

from src.utils.utils import pyt2np


import logging
logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)

HALF_PRECISION = True

class Trainer:
    def __init__(
        self,
        model,
        loss_fn,
        optimizer,
        data_loader_train,
        data_loader_val,
        data_loader_test,
        exp_dir,
        dev,
    ):

        self.data_loader_train = [x for x in tqdm(data_loader_train)]
        self.data_loader_val = [x for x in tqdm(data_loader_val)] # data_loader_val
        self.data_loader_test = [x for x in tqdm(data_loader_test)]# data_loader_test
        self.model = model
        self.opt = optimizer
        
        self.scaler = torch.cuda.amp.GradScaler()

        self.dev = dev
        self.loss_fn = loss_fn
        self.print_freq = 100  # Update print frequency
        self.save_freq = 10
        self.exp_dir = exp_dir
        

    def one_pass(self, data_loader, phase, preds_storage=None):
        """
        Performs one pass on the entire training or validation dataset
        """
        model = self.model
        opt = self.opt
        loss_fn = self.loss_fn

        loss_tot = 0
        
        summary = collections.defaultdict(float)
        
        
        for it, batch in enumerate(data_loader):
            # Zero the gradients of any prior passes
            opt.zero_grad()
            # Send batch to device
            batch = self.send_to_device(batch)
            
            with torch.cuda.amp.autocast(enabled=(phase=="train" and HALF_PRECISION)):
                # Forward pass
                output = model(batch)
                # Compute loss
                losses = loss_fn(output, batch, phase)
                
                for k in losses:
                    summary[k] += losses[k]
            
            if losses["loss"].requires_grad:
                if HALF_PRECISION:
                    self.scaler.scale(losses["loss"]).backward()
                    self.scaler.step(opt)
                    self.scaler.update()
                else:
                    # Backward pass, compute the gradients
                    losses["loss"].backward()
                    # Update the weights
                    opt.step()

            # WARNING: This is only accurate if batch size is constant for all iterations
            # If drop_last=True, then this is not the case for the last batch.
            loss_tot += losses["loss"].detach()

            if (it % self.print_freq) == 0:
                self.print_update(losses, it, len(data_loader))
                # NOTE You may want to add visualization and a logger like
                # tensorboard, w&b or comet

            if not preds_storage is None:
                preds_storage.append(output["kp3d"])
        
        for k in summary:
            summary[k] /= len(data_loader)
        
        logging.debug("Finnished one pass, summary:")
        self.print_update(summary, it, len(data_loader))
        
        loss_tot /= len(data_loader)

        return loss_tot, preds_storage, summary

    def send_to_device(self, batch):
        nb = {}
        for k, v in batch.items():
            nb[k] = v.to(self.dev)
        return nb

    def print_update(self, losses, it, n_iter):
        str_print = f"Iter: {it:04d}/{n_iter:04d}\t"
        for loss_name, loss_value in losses.items():
            str_print += f"{loss_name}: {loss_value:0.4f}\t"
        str_print = str_print[:-2]  # Remove trailing tab
        logging.debug(str_print)

    def train_model(self, n_epochs):
        best_perf_metric = 99999.0
        for e in range(n_epochs):
            
            print(f"\nEpoch: {e+1:04d}/{n_epochs:04d}")
            # Train one epoch
            self.model.train()
            print("##### TRAINING #####")
            self.one_pass(self.data_loader_train, phase="train")
            if e + 1 == 125:
                for g in self.opt.param_groups:
                    g['lr'] /= 10
                    logging.info(g['lr'])
                    
            # Evaluate on validation set
            with torch.no_grad():
                self.model.eval()
                print("##### EVALUATION #####")
                _, _, summary = self.one_pass(self.data_loader_val, phase="eval")

            if summary["perf_metric"] < best_perf_metric:
                best_perf_metric = summary["perf_metric"]
                logging.info("Best perf_metric: %f \n saving..."%(best_perf_metric, ))
                # NOTE You may want to store the best performing model based on the
                # validation set in addition
                torch.save(
                    self.model.state_dict(),
                    os.path.join(self.exp_dir, f"model_{e:04d}.pt"),
                )

        torch.save(
            self.model.state_dict(), os.path.join(self.exp_dir, f"model_last.pt")
        )

    def test_model(self):
        """
        Runs model on testing data
        """
        print("##### TESTING #####")
        # NOTE If you are saving the best performing model, you may want to first load
        # the its weights before running test
        with torch.no_grad():
            _, preds_storage, _ = self.one_pass(
                self.data_loader_test, phase="test", preds_storage=[]
            )

        preds = pyt2np(torch.cat(preds_storage, dim=0)).tolist()

        test_path = os.path.join(self.exp_dir, "test_preds.json")
        print(f"Dumping test predictions in {test_path}")
        with open(test_path, "w") as f:
            json.dump(preds, f)
        subprocess.call(['gzip', test_path])


